# Short-Term Precipitation Forecasting from Satellite Data Using Machine Learning

![WeatherFusionNet](thesis/src/figures/weatherfusionnet.png)

This is my Master's thesis, and a solution to the Weather4cast 2022 competition. For more info about the competition, description of the used data and a baseline starter kit, please see [iarai/weather4cast-2022](https://github.com/iarai/weather4cast-2022). This repository is a fork of the starter kit.

## Usage instructions

Download the data ([available here](https://www.iarai.ac.at/weather4cast/forums/topic/weather4cast-2022-stage-2-data-access/)) and extract it into the `data` subfolder, or edit `models/configurations/config.yaml` to point to the right folder.

Install dependencies with
```
conda env create -f environment.yml
conda activate weather4cast
```

## Files
- `models` folder - model implementations
- `weights` folder - trained model weights
- `utils` folder - data loader, mostly from the starter kit
- Training scripts
    - `train.py`
    - `train-sat2rad.py`
    - `train-sat-phydnet.py`
    - `train-radar2radar.py`
- Visualizations notebooks
    - `visualize.ipynb`
    - `visualize-sat2rad.ipynb`
    - `visualize-optical-flow.ipynb`
    - `visualize-radar2radar.ipynb`

## Generating a submission

The `predict-submission.py` script generates a submission zip file for a given challenge and split. For example:
```
python predict-submission.py --challenge core --split test --gpus 0
```
or
```
python predict-submission.py --challenge transfer --split heldout --gpus 0
```
You can find the result in `submission/submission.zip`.

See `python predict-submission.py --help` for more info on the arguments.

