from matplotlib import pyplot as plt
from matplotlib.animation import ArtistAnimation
from IPython.core.display import HTML, display
from PIL import Image
import numpy as np


def artist_animation(fig, frames, **kwargs):
    anim = ArtistAnimation(fig, frames, **kwargs)
    plt.close()
    display(HTML(anim.to_jshtml()))


def animate(frames, figsize=None, **args):
    fig = plt.figure(figsize=figsize)
    artist_animation(fig, [[plt.imshow(t, **args)] for t in frames])


def rescale(arr: np.ndarray, shape: tuple):
    return np.asarray(Image.fromarray(np.nan_to_num(arr)).resize(reversed(shape[-2:])))