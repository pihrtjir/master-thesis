from utils.data_utils import load_config
import torch
import numpy as np
import pysteps
import einops
from models.base_module import BaseLitModule



class UNetWrapper(torch.nn.Module):
    """
    Pads the input to be divisible by 32, and crops the output back to the original dimensions
    """
    def __init__(self, input_channels, output_channels, nb_filter=None):
        super().__init__()
        self.input_channels = input_channels
        from models.backbones.unet import UNet
        self.model = UNet(input_channels=input_channels, num_classes=output_channels, nb_filter=nb_filter)

    def forward(self, x):
        img_w = x.shape[-2]
        img_h = x.shape[-1]

        pw = (32 - img_w % 32) // 2
        ph = (32 - img_h % 32) // 2

        x = x.reshape(-1, self.input_channels, img_w, img_h)
        x = torch.nn.functional.pad(x, (pw, pw, ph, ph), mode="replicate")  # 252x252 -> 256x256
        x = self.model(x)
        x = x.unsqueeze(1)  # add back channel dim
        x = x[..., pw:-pw, ph:-ph]  # back to 252x252
        return x


def crop_slice(img_size=252, scale_ratio=2/12):
    padding = int(img_size * (1 - scale_ratio) // 2)
    return ..., slice(padding, img_size-padding), slice(padding, img_size-padding)


class PhyDNetWrapper(torch.nn.Module):
    def __init__(self, config_path, ckpt_path=None):
        super().__init__()
        phydnet_config = load_config(config_path)
        from models.backbones.phydnet import PhyDNet
        if ckpt_path:
            self.phydnet = PhyDNet.load_from_checkpoint(ckpt_path, config=phydnet_config)
        else:
            self.phydnet = PhyDNet(phydnet_config)

    def forward(self, x):
        return self.phydnet(x)



# ---------------------------------
# LIGHTNING MODULES
# ---------------------------------


class UNet(BaseLitModule):
    def __init__(self, config):
        super().__init__(config)

        self.unet = UNetWrapper(
            input_channels=11 * config["dataset"]["len_seq_in"],
            output_channels=config["dataset"]["len_seq_predict"],
        )

    def forward(self, x, metadata=None):
        return self.unet(x)


class UNetCropUpscale(BaseLitModule):
    def __init__(self, config):
        super().__init__(config)

        in_channels = 11 * config["dataset"]["len_seq_in"]
        if config["dataset"]["load_topo"]: in_channels += 1
        if config["dataset"]["load_lat_long"]: in_channels += 2
        self.unet = UNetWrapper(
            input_channels=in_channels,
            output_channels=config["dataset"]["len_seq_predict"],
        )
        self.upscale = torch.nn.Upsample(scale_factor=6, mode='bilinear', align_corners=True)

    def forward(self, x, metadata=None, crop=True, upscale=True):
        if metadata is not None:
            x = torch.concatenate([x.flatten(1, 2), metadata["topo"], metadata["lat-long"]], dim=1)
        else:
            x = x.flatten(1, 2)
            
        x = self.unet(x)

        if crop:
            x = x[crop_slice()]

        if upscale:
            x = self.upscale(x[:, 0]).unsqueeze(1)

        return x


class WeatherFusionNet(BaseLitModule):
    def __init__(self, config):
        super().__init__(config)
        self.phydnet = PhyDNetWrapper("models/configurations/phydnet.yaml", ckpt_path="weights/sat-phydnet.ckpt")

        self.sat2rad = UNetWrapper(input_channels=11, output_channels=1)
        self.sat2rad.load_state_dict(torch.load("weights/sat2rad-unet.pt"))

        self.unet = UNetWrapper(input_channels=11 * (4 + 10) + 4, output_channels=32)
        self.upscale = torch.nn.Upsample(scale_factor=6, mode='bilinear', align_corners=True)

    def forward(self, x, metadata=None, return_inter=False):
        self.sat2rad.eval()
        with torch.no_grad():
            sat2rad_out = self.sat2rad(x.swapaxes(1, 2)).reshape(x.shape[0], 4, x.shape[-2], x.shape[-1])

        self.phydnet.eval()
        with torch.no_grad():
            phydnet_out = self.phydnet(x.swapaxes(1, 2)).flatten(1, 2)

        x = torch.concat([x.flatten(1, 2), phydnet_out, sat2rad_out], dim=1)
        unet_out = x = self.unet(x)
        x = x[crop_slice()]
        x = self.upscale(x[:, 0]).unsqueeze(1)

        if return_inter:
            return sat2rad_out, phydnet_out, unet_out, x
        return x


class WFN_Static(BaseLitModule):
    def __init__(self, config):
        super().__init__(config)
        self.phydnet = PhyDNetWrapper("models/configurations/phydnet.yaml", ckpt_path="weights/sat-phydnet.ckpt")

        self.sat2rad = UNetWrapper(input_channels=14, output_channels=1)
        self.sat2rad.load_state_dict(torch.load("weights/sat2rad-unet-static.pt"))

        in_channels = 11 * (4 + 10) + 4
        if config["dataset"]["load_topo"]: in_channels += 1
        if config["dataset"]["load_lat_long"]: in_channels += 2
        self.unet = UNetWrapper(input_channels=in_channels, output_channels=32)
        self.upscale = torch.nn.Upsample(scale_factor=6, mode='bilinear', align_corners=True)

    def forward(self, x, metadata, return_inter=False):
        self.sat2rad.eval()
        with torch.no_grad():
            sat2rad_in = torch.concatenate([
                x.swapaxes(1, 2), # b, t, c
                metadata["topo"].unsqueeze(1).repeat(1, 4, 1, 1, 1), # b, c -> b, 4, c
                metadata["lat-long"].unsqueeze(1).repeat(1, 4, 1, 1, 1), # b, c -> b, 4, c
            ], dim=2)
            sat2rad_out = self.sat2rad(sat2rad_in).reshape(x.shape[0], 4, x.shape[-2], x.shape[-1])
            sat2rad_out = torch.sigmoid(sat2rad_out)

        self.phydnet.eval()
        with torch.no_grad():
            phydnet_out = self.phydnet(x.swapaxes(1, 2)).flatten(1, 2)

        x = torch.concat([x.flatten(1, 2), sat2rad_out, phydnet_out, metadata["topo"], metadata["lat-long"]], dim=1)
        unet_out = x = self.unet(x)
        x = x[crop_slice()]
        x = self.upscale(x[:, 0]).unsqueeze(1)

        if return_inter:
            return sat2rad_out, unet_out, x
        return x
    

class Sat2RadOpticalFlow(BaseLitModule):
    def __init__(self, config):
        super().__init__(config)

        self.unet = UNetWrapper(
            input_channels=11,
            output_channels=1,
            nb_filter=config["model"]["nb_filter"],
        )
        self.unet.load_state_dict(torch.load("weights/sat2rad-unet.pt"))
        self.upscale = torch.nn.Upsample(scale_factor=6, mode='bilinear', align_corners=True)

    def forward(self, x, metadata=None, return_inter=False):
        x = einops.rearrange(x, "b c t w h -> (b t) c w h")
        x = self.unet(x)
        x = einops.rearrange(x, "(b t) 1 1 w h -> b t w h", t=4)
        sat2rad_out = x

        x = x.detach().cpu().numpy().astype(np.float64)
        flow = np.stack([pysteps.motion.lucaskanade.dense_lucaskanade(x[i], verbose=False) for i in range(len(x))])
        x = np.stack([pysteps.nowcasts.extrapolation.forecast(x[i, -1], flow[i], self.config["dataset"]["len_seq_predict"]) for i in range(len(x))])
        x = np.nan_to_num(x, nan=np.nanmin(x))
        x = torch.FloatTensor(x).to(self.device)
        before_crop = x

        x = x[crop_slice()]
        x = self.upscale(x)
        x = x.unsqueeze(1)

        if return_inter:
            return sat2rad_out, flow, before_crop, x
        return x


class Sat2RadPersistence(BaseLitModule):
    def __init__(self, config):
        super().__init__(config)

        self.unet = UNetWrapper(
            input_channels=11,
            output_channels=1,
            nb_filter=config["model"]["nb_filter"],
        )
        self.unet.load_state_dict(torch.load("weights/sat2rad-unet.pt"))
        self.upscale = torch.nn.Upsample(scale_factor=6, mode='bilinear', align_corners=True)

    def forward(self, x, metadata=None, return_inter=False):
        x = self.unet(x[:, :, -1])
        x = x[crop_slice()]
        x = self.upscale(x[:, 0])
        x = x.unsqueeze(1)
        x = x.repeat(1, 1, 32, 1, 1)
        return x


class Radar2RadarUNet(BaseLitModule):
    def __init__(self, config):
        super().__init__(config)
        self.unet = UNetWrapper(
            input_channels=config["dataset"]["len_seq_in"],
            output_channels=config["dataset"]["len_seq_predict"],
        )

    def forward(self, x, metadata=None):
        return self.unet(x)
